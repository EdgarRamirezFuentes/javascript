/*
* Ramírez Fuentes Edgar Alejandro
*/

/**
* Eleva el número base a la potencia deseada
* @param {Number} base es el número base que se va a elevar
* @param {Number} es la potencia a la que se va a elevar el número base
* @return el resultado de elevar el número base a la potencia deseada
*/
const potencia = (base, exponente) => Math.pow(base, exponente);

/**
* Imprime el resultado de elevar el número base a la potencia deseada
* @param {Number} base es el número base que se va a elevar
* @param {Number} es la potencia a la que se va a elevar el número base
*/
(function (base, exponente) {
  console.log("Funcion anonima")
  console.log(Math.pow(base, exponente));
}
)(4,3);


console.log("Funcion flecha");
console.log(potencia(3,2));