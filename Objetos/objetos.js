/*
    Ramírez Fuentes Edgar Alejandro
*/
const Calculadora = function () {
    this.suma = function (a, b) { return a + b; };
    this.resta = function (a, b) { return a - b; };
}

const CalcAvanzada = function () {
    this.multiplicacion = function (a, b) { return a * b ; };
    this.division = function (a, b) { return a / b; };
}

CalcAvanzada.prototype = new Calculadora();

let cal = new CalcAvanzada();
console.log(cal.suma(2,3));
console.log(cal.resta(2,3));
console.log(cal.multiplicacion(2,3));
console.log(cal.division(2,3));  
