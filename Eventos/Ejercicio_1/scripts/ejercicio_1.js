/* 
    Ramírez Fuentes Edgar Alejandro
*/
// Espera a que el DOM esté listo
document.addEventListener('DOMContentLoaded', (e) => {
    // Obtenemos el elemento con id="boton"
    const boton = document.getElementById("boton");
    // Obtenemos el elemento con id="imagen"
    const imagen = document.getElementById("imagen");
    // Añadimos un evento click al botón
    boton.addEventListener('click', (e) => {
        alert("Has dado click al botón");
    });
    // Añadimos un evento mouseover al elemento con id="imagen"
    imagen.addEventListener("mouseover", (e) => {
        alert("Has pasado el cursor sobre la imagen");
    });
});