/* 
    Ramírez Fuentes Edgar Alejandro
*/
// Espera a que el DOM esté listo
document.addEventListener('DOMContentLoaded', (e) => {
    function notificar_click () { alert('Has dado click en el botón'); }
    function notificar_mouseover () { alert("Has pasado el cursor sobre la imagen"); }
    // Obtenemos el elemento con id="boton"
    const boton = document.getElementById("boton");
    // Obtenemos el elemento con id="imagen"
    const imagen = document.getElementById("imagen");
    // Añadimos un evento click al botón
    boton.onclick = notificar_click;
    // Añadimos un evento mouseover al elemento con id="imagen"
    imagen.onmouseover = notificar_mouseover;
});