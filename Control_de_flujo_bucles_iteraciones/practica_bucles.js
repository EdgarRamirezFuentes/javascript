/**
 * Ramírez Fuentes Edgar Alejandro
 * 10/25/2021
 */

let tabla = 8;
let altura = 7;

/**
 * Imprime la tabla de multiplicar hasta el 10 del valor recibido
 * @param {Number} valor es el valor del cual se imprimirá la table de multiplicar 
 */
function tabla_multiplicacion(valor) {
    console.log("Tabla de multiplicacion de " + valor);
    for (let i = 0; i <= 10; i++) {
        console.log(valor + " * " + i + " = " + (valor * i));
    }
}

/**
 * Imprime un triangulo con la altura recibida
 * @param {Number} altura es la altura que tendrá el triángulo 
 */
function construir_triangulo(altura) {
    let altura_actual = 0;
    while (altura_actual <= altura) {
        let piso = "*";
        for (let i = 1; i <= altura_actual; i++) {
            piso += "*";
        }
        console.log(piso);
        altura_actual++;
    }
}
tabla_multiplicacion(tabla);
construir_triangulo(altura);