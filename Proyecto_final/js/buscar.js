$(document).ready(function(){
	// Utilizado para mostrar las coincidencias de la busqueda
	const contenedor_resultados = $("#contenedor_resultados");
	
	// Utilizado para obtener el valor de la busqueda
	const busqueda = $("#busqueda");
	const btn_buscar = $("#btn_buscar");

	const lista = $("#mi_lista");
	const contenedor_lista = $("#contenido_lista");
	contenedor_lista.hide();

	// Muestra los mensajes que avisan al usuario el número de coincidencias de una busqueda
	const mensaje_busqueda = $("#mensaje_busqueda");
	const btn_lista = $('#btn_lista');

	// Clase utiizada para llevar el control de la lista de películas del usuario
	// Utilizará localStorage para almacenar la lista del usuario
	const Lista_peliculas = function () {
		/** Objeto encargado de almacenar las películas de la lista */
		this.lista = {}

		/** Carga películas existentes en la lista, si es que estas existen */
		this.cargar_lista = function () {
			movies_data = localStorage.getItem("lista");
			this.lista = (movies_data !== null ? JSON.parse(movies_data) : {});
		}

		/**
		 * 
		 * @param {Number} id es el ID correspondiente a la película
		 * @param {string} titulo es el título de la película
		 */
		this.agregar_pelicula = function (id, titulo) {
			// La agrega únicamente si no existe en la lista
			if (this.lista[id] === undefined) {
				// Actualiza la lista en localStorage
				this.lista[id] = titulo;
				localStorage.setItem("lista", JSON.stringify(this.lista));
				// Integra la película a la lista
				let pelicula_nueva = `<li class="dropdown-item">${titulo}</li>`
				lista.append(pelicula_nueva);
				show_custom_alert("success", "¡Éxito!", `${titulo} fue agregada correctamente a tu lista`);
			} else {
				show_custom_alert("warning", "¡Cuidado!", `${titulo} ya existe en tu lista`);
			}
		}

		/**
		 * Agrega los elementos HTML en la lista por cada película
		 */
		this.actualizar_lista = function () {
			lista.children().remove();
			if (this.lista.length === 0) {
				lista.html("Tu lista está vacía");
			} else{
				lista.before('<p>Mi lista de películas</p>');
				$.each(this.lista, function (index, movie) {
					let list_item = `<li class="dropdown-item">${movie}</li>`
					lista.append(list_item);
				});
			}
		}
	}

	// Inicializamos el estado principal de la página
	mi_lista = new Lista_peliculas();
	mi_lista.cargar_lista();
	mi_lista.actualizar_lista();

	/**
	 * EventListener encargado de estar a la escucha del botón que muestra/oculta su lista de películas
	 */
	btn_lista.click(function (e) {
		if (contenedor_lista.is(":visible")) {
			contenedor_lista.hide(1500);
			btn_lista.html("Mostrar lista");
		} else {
			contenedor_lista.show(1500);
			btn_lista.html("Ocultar lista");
		}
	});

	/**
	 * EventListener encargado de estar a la eschucha del botón que realizar una busqueda de películas
	 */
	btn_buscar.click(function (e) { 
		const palabra = busqueda.val();
		if (palabra != "") {
			const request_url =`https://api.themoviedb.org/3/search/movie?api_key=3356865d41894a2fa9bfa84b2b5f59bb&language=es&query=${palabra}`;
			$.ajax({
				type: "get",
				url: request_url,
				dataType: "json",
				beforeSend: function () {
					mensaje_busqueda.hide();
					mensaje_busqueda.html(`Buscando coincidencias para "${palabra}"`);
					mensaje_busqueda.show(300);
					contenedor_resultados.html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
				},
				success: function ({results: resultados}) {
					$(".contenedor_pelicula").remove();
					setTimeout(() => {
						$("#loader").remove();
						mensaje_busqueda.hide();
						mensaje_busqueda.html(resultados.length > 1 || resultados.length === 0 ? `Se encontraron ${resultados.length} coincidencias para "${palabra}"` : `Se encontró ${resultados.length} coincidencia para "${palabra}"`);
						mensaje_busqueda.show(500);
						$.each(resultados, function (index, pelicula) { 
							contenedor_resultados.append(construir_contenedor_pelicula(pelicula));
						});
					}, 3000);
				},
				error : function () {
					const mensaje_error = `<p>Ocurrió un problema al intentar buscar ${palabra}</p>`
					contenedor_resultados.append(mensaje_error);
				}
				
			});
		} else {
			// Limpia el área de muestreo de elementos de busqueda
			contenedor_resultados.children(".contenedor_pelicula").remove();
			mensaje_busqueda.html(`Debes de ingresar el nombre de la película a buscar`);
		}
	});

	/**
	 * EventListener encargado de estar al pendiente de cuando alguno de los botones
	 * creados en cada contenedor de película hay sido presionado para agregar la información
	 * de dicha película a la lista.
	 */
	$(document).on('click', '.btn_agregar', function (e) { 
		// Obtiene el padre del botón para obtener los datos necesarios para agregarlos a la lista
		const padre_boton = $(this).parent();
		// Obtiene el id de la pelicula a agregar (id del padre)
		const id = padre_boton.attr("id");
		// Se obtiene el valor del título de la película a agregar
		const {outerText: titulo} = padre_boton.children(":first")[0];
		mi_lista.agregar_pelicula(id, titulo);
	});

	/**
	 * 
	 * @param {Number} id es el id de la película retornado por la API
	 * @param {string} titulo es el título de la película
	 * @param {string} ruta_poster es el enlace proporcionado por la API para obtener el poster de la película
	 * @returns una string con la estructura del contenedor para la película en cuestión
	 */
	function construir_contenedor_pelicula ({id, title : titulo, poster_path: ruta_poster}) {
		// Asigna la ruta del poster o una imagen por default si no existe un poster
		const img_url = ruta_poster ? `https://image.tmdb.org/t/p/w500/${ruta_poster}` : "./images/no_image.jpg";
		const contenedor_pelicula = `
			<!-- contenedor de ${titulo} -->
			<div id="${id}" class="contenedor_pelicula">
				<div id="${titulo}_info" class ="info_pelicula">
					<div class="contenedor_poster">
						<img src="${img_url}" class="poster_pelicula rounded" alt ="${titulo} poster">
					</div>
					<p class="titulo_pelicula">${titulo}</p>
				</div>
				<button id="btn${id}" class="btn btn-primary w-75 btn_agregar">Agregar a mi lista</button>
			</div>
			<!-- Fin contenedor de ${titulo} -->
		`;
		return contenedor_pelicula;
	}
});

/**
 * Muestra un alert personalizado de sweetalert2 - https://sweetalert2.github.io/#usage
 * 
 * @param {string} es el tipo de ícono que se mostrará en el alert
 * @param {string} titulo del alert
 * @param {string} msg del alert
 */
function show_custom_alert (tipo, titulo, msg) {
	Swal.fire(
		titulo,
		msg,
		tipo
	);
}