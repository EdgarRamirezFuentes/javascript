# Video Risa

Fue una práctica final muy interesante porque tuve que poner en práctica las habilidades previamente adquiridas
para tratar de brindar un producto útil para el usuario.
Hice una pequeña modificación con respecto a lo pedido para proveer al usuario un poco de libertad al momento
de seleccionar que películas agregar a su lista.

Al hacer la petición a la API con la palabra "transformers" me di cuenta de que se retorna una arreglo con 
películas que de alguna manera coindicen con la palabra enviada, y me pareció buena idea permitir a mi usuario
elegir entre todas las películas retornadas aquellas que quería agregar a su lista. Es por eso que, decidí mostrar
cada una de las coindidencias en un contenedor con un botón que permite agregar las películas a la lista.

Además, me pareció una buena idea implementar una "base de datos" para llevar el registro de las películas que 
el usuario ha agregado hasta el momento. Ante la falta de uso de base de datos con JS opté por usar el localStorage del
navegador para almacenar esta lista y tenerla cargada cada que se abra el sitio.

Hasta el momento solo agrega películas a la lista, pero en un futuro se podría implementación de la eliminación de películas de
la lista.